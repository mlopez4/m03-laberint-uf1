# M03 Laberint UF1

## Projecte de M03.
### Desenvolupament d'un joc laberint.
----
(S'ha "**intentat**" fer servir una arquitectura *vista/model/control*.)

----
El joc consta d'un menu d'inici on podem derivar a les diferent seccions.

* Principal
    * Guardar un taulell personalitzat (JSON)
* Jugar
    * Demana Nom
    * Demana Nivell
    * Demana Taulell
    * Demana Moviments fins que arribi al final
    * Sortir
* Veure puntuacions
    * Mostra puntuacions ordenades de millors a pitjors. (JSON)
* Sortir

----

## Descripcions del mòduls mes importants

En [General.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/view/General.java) i [Igrafiques.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/view/Igrafiques.java) he desenvolupat els moduls relacionats amb la impresio de informacio per pantalla.

Tenim dos arxius de gestio de enmagatzematge que s'encarregan de guardar i llegir tant taulells com puntuacios en fitxers JSON: [GestionarPuntuacio.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/model/GestionarPuntuacio.java) i [GestionarTaulells.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/model/GestionarTaulells.java).

Tota la informació de la partida en actiu s'enmagatzema en un objecte: [Partida.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/model/Partida.java).

L' entrada de dades es gestionada per el fitxer [Entradas.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/model/Entradas.java) encarregat de fer les validacions pertinents en cada tipus de solicitud.

La "dinamica" del programa en general es desenvolupada per els arxius [Jugar.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/control/Jugar.java) i [Main.java](https://gitlab.com/mlopez4/m03-laberint-uf1/blob/master/src/control/Main.java), un encarregat de la dinamica del a partida i l'altra del "cos general" del joc. 

----

## Joc de proves

Per fer els jocs de proves s'han declarat les funcions pertinents individualment en el mètode main i s'ha anat introduint dades sensibles d'errors per veure els resultats.

On s'ha posat més atenció a sigut en tenir en compte els límits del taulell perquè no ens retorni excepcions del tipus outOfIndex.

----

## Millores

Per falta de temps no s'han fet millores, però hi tenia pensades vàries faci'ls de desenvolupar:

* Millora de la interfície gràfica.
* Millora de la verificació en la introducció de dades.
* Canviar els caràcters del taulell (són poc intuïtius) per:
    * x - territori sense explorar
    * · - camí recorregut
    * o - posició del personatge
    * | - mur
* Marcar la posició del personatge amb una "o" per saber en tot moment on ens trobem.
