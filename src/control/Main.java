package control;


import model.Entradas;
import model.GestionarPuntuacio;
import model.GestionarTaulells;
import view.General;
import model.Partida;

public class Main {

    public static void main(String[] args) {

        General.titol();
        char[] opcionsPrincipals = {'c','j','r','s'};
        boolean sortir=false;
        while (sortir==false) {
            General.menuOpcions();
            char opcio = Entradas.demanarCaracter(opcionsPrincipals);
            switch (opcio){
                case 'c':
                    System.out.println();
                    System.out.println();
                    General.subtitol("DIBUIXA EL TEU TAULELL");
                    GestionarTaulells getio = new GestionarTaulells();
                    getio.guardar(Entradas.demanarTaulell());
                    break;
                case 'j':
                    GestionarTaulells gestio = new GestionarTaulells();
                    System.out.println();
                    System.out.println();
                    General.subtitol("INICIAR PARTIDA");
                    System.out.println("Introdueix el teu nom:");
                    String nom = Entradas.demanarNom();
                    String[] nivells = {"(F)ACIL", "(M)ITJÀ", "(D)IFICIL"};
                    General.menu(nivells,"NIVELLS");
                    char[] opcions ={'f','m','d'};
                    char nv = Entradas.demanarCaracter(opcions);
                    char[][] taulell = gestio.carregar(nv);
                    Partida partida = new Partida(taulell, nom);
                    Jugar.moviment(partida);
                    break;
                case 'r':
                    GestionarPuntuacio punts = new GestionarPuntuacio();
                    punts.mostrarPuntuacions();
                    break;
                case 's':
                    sortir = true;
                    break;
            }
        }





    }

}
