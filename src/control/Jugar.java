package control;
import model.GestionarPuntuacio;
import model.Partida;
import model.Entradas;
import view.General;

public class Jugar {

  public static void moviment(Partida p){
    boolean sortir = false;
    boolean fi = false;
    String[] noms = {"a - ESQUERRA","s - AVALL", "d - DRETA","w - AMUNT","x - ABANDONAR"};
    char[] opcions = {'a','s','d','w','x'};

    while (sortir==false && fi==false){

      General.mostrarTaulell(p.getTaulellJugador());
      System.out.println("JUGADOR: "+p.getNomJugador()+" | MOVIMENTS: "+p.getMoviments());
      System.out.println(p.getNivell());

      char escollit = Entradas.demanarCaracter(opcions);
      switch (escollit){
        case 'a':
          fi = p.esquerra();
          break;
        case 's':
          fi = p.abaix();
          break;
        case 'd':
          fi = p.dreta();
          break;
        case 'w':
          fi = p.amunt();
          break;
        case 'x':
          sortir = true;
          break;
      }
    }

    if (fi==true){
      General.subtitol("FELICITATS");
      General.subtitol("Has sortir del laberint en "+p.getMoviments()+" moviments");
      GestionarPuntuacio save = new GestionarPuntuacio();
      save.guardarPuntuacio(p);
      General.subtitol("Puntuacio guardada correctament.");
    }

  }

}
