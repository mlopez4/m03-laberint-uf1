package view;

public class General {

  static final int AMPLADA_PANT = 100;
  static final char PINCELL_GR = '*';
  static final char PINCELL_PT = '-';

  static final String TITOL_INICIAL = " BENVINGUTS AL JOC DEL LABERINT ";

  static final String TITOL_MENU = "  MENU PRINCIPAL  ";
  static final String[] MENU_INICIAL = {"(C)ARREGAR TAULELL", "(J)UGAR PARTIDA", "(R)ESULTATS PARTIDES", "(S)ORTIR"};

  /**
   * Printa el titol principal
   */
  public static void titol(){


    int pintar_fons = (AMPLADA_PANT - TITOL_INICIAL.length())/2;

    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.println();
    for (int i=0; i<pintar_fons; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.print(TITOL_INICIAL);
    for (int i=0; i<pintar_fons; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.println();
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.println();
  };


  /**
   * Printa les opcions principals
   */
  public static void menuOpcions() {

    int costats = (AMPLADA_PANT - TITOL_MENU.length()) / 2;

    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();
    for (int i=0; i<costats; i++){
      System.out.print(PINCELL_PT);
    }

    System.out.print(TITOL_MENU);

    for (int i=0; i<costats; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();

    for (int i=0; i<MENU_INICIAL.length; i++){
      String opcio = MENU_INICIAL[i];
      costats = (AMPLADA_PANT - opcio.length()) / 2;
      for (int j=0; j<costats; j++){
        System.out.print(PINCELL_PT);
      }
      System.out.print(opcio);
      for (int j=0; j<costats; j++){
        System.out.print(PINCELL_PT);
      }
      System.out.println();
    }
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }

  }


  /**
   * printa taulell donat per parametre
   * @param taulell
   */
  public static void mostrarTaulell(char[][] taulell){
    for (int i=0; i<taulell.length; i++){
      for (int j=0; j<taulell[i].length; j++){
        System.out.print(taulell[i][j]+" ");
      }
      System.out.println();
    }
  }

  /**
   * Printa subtitol donat per parametre
   * @param text
   */
  public static void subtitol(String text){

    int costats = (AMPLADA_PANT - text.length()) / 2;

    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();
    for (int i=0; i<costats; i++){
      if (i==0){
        System.out.print("|");
      }else {
        System.out.print(" ");
      }
    }

    System.out.print(text);

    for (int i=0; i<=costats; i++){
      if (i==costats){
        System.out.print("|");
      }else {
        System.out.print(" ");
      }
    }
    System.out.println();
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();
  }


  /**
   * Printa menu d'opcions donades per parametre
   * @param apartats
   * @param titol
   */
  public static void menu(String[] apartats, String titol){

    int costats = (AMPLADA_PANT - TITOL_MENU.length()) / 2;

    //PRIMERA LINEA
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();

    //COSTAT ESQUERRE
    for (int i=0; i<costats; i++){
      System.out.print(PINCELL_PT);
    }

    // TITOL
    System.out.print(titol);

    //COSTAT DRET
    for (int i=0; i<costats; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();

    //LINIA SEPARADORA
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();

    //COMEÇA EL MENU
    for (int i=0; i<apartats.length; i++){
      String opcio = apartats[i];
      costats = (AMPLADA_PANT - opcio.length()) / 2;
      for (int j=0; j<costats; j++){
        System.out.print(PINCELL_PT);
      }
      //PRINTA OPCIO
      System.out.print(opcio);
      for (int j=0; j<costats; j++){
        System.out.print(PINCELL_PT);
      }
      System.out.println();
    }
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();
  }


}
