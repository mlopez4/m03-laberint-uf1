package view;

public class Igrafiques {

  static final int AMPLADA_PANT = 100;
  static final char PINCELL_GR = '*';
  static final char PINCELL_PT = '-';

  static final String TITOL_INICIAL = " BENVINGUTS AL JOC DEL LABERINT ";

  static final String TITOL_MENU = "  MENU PRINCIPAL  ";
  static final String[] MENU_INICIAL = {"(C)ARREGAR TAULELL", "(J)UGAR PARTIDA", "(R)ESULTATS PARTIDES", "(S)ORTIR"};


  public static void titol(){


    int pintar_fons = (AMPLADA_PANT - TITOL_INICIAL.length())/2;

    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.println();
    for (int i=0; i<pintar_fons; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.print(TITOL_INICIAL);
    for (int i=0; i<pintar_fons; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.println();
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_GR);
    }
    System.out.println();
  };

  public static void menuOpcions() {

    int costats = (AMPLADA_PANT - TITOL_MENU.length()) / 2;

    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();
    for (int i=0; i<costats; i++){
      System.out.print(PINCELL_PT);
    }

    System.out.print(TITOL_MENU);

    for (int i=0; i<costats; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }
    System.out.println();

    for (int i=0; i<MENU_INICIAL.length; i++){
      String opcio = MENU_INICIAL[i];
      costats = (AMPLADA_PANT - opcio.length()) / 2;
      for (int j=0; j<costats; j++){
        System.out.print(PINCELL_PT);
      }
      System.out.print(opcio);
      for (int j=0; j<costats; j++){
        System.out.print(PINCELL_PT);
      }
      System.out.println();
    }
    for (int i=0; i<AMPLADA_PANT; i++){
      System.out.print(PINCELL_PT);
    }

  }

  public static void mostrarTaulell(char[][] taulell){
    for (int i=0; i<taulell.length; i++){
      for (int j=0; j<taulell[i].length; j++){
        System.out.print(taulell[i][j]);
      }
      System.out.println();
    }
  }

}
