package model;

// LLIBRERIES PER LLEGIR

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


public class GestionarTaulells {




  static final int MAX_FACIL = 7;
  static final int MAX_MEDIA = 15;



  private JSONArray nivells;





  public GestionarTaulells (){
    JSONParser parser = new JSONParser();
    try {
      File fitxer = new File("res/taulells.json");
      System.out.println("El taulell es guardara a la ruta: "+fitxer.getAbsolutePath());
      FileReader f = new FileReader(fitxer);
      nivells = (JSONArray) parser.parse(f);
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Error de lectura");
    }
  }





  public char[][] carregar(char nivell) {

    char[][] taulell;

    JSONArray nivellEscollit = null;
    String nomNivell = null;
    int numeroTaulells = 0;

    switch (nivell) {
      case 'f':
        nivellEscollit = (JSONArray) nivells.get(0);
        nomNivell = "facil";
        break;
      case 'm':
        nivellEscollit = (JSONArray) nivells.get(1);
        nomNivell = "mitjà";
        break;
      case 'd':
        nivellEscollit = (JSONArray) nivells.get(2);
        nomNivell = "dificil";
        break;
    }


    numeroTaulells = nivellEscollit.size();

    System.out.println("Escull un taulell del 1 al " + numeroTaulells + " del nivell " + nomNivell);

    int op = Entradas.demanarEnter(1, numeroTaulells);

    JSONArray taulellEscollit = (JSONArray) nivellEscollit.get(op - 1);
    JSONArray filaTaulellEscollit = (JSONArray) taulellEscollit.get(0);

    int files = taulellEscollit.size();
    int columnes = filaTaulellEscollit.size();

    taulell = new char[files][columnes];

    for (int i = 0; i < files; i++) {
      JSONArray fila = (JSONArray) taulellEscollit.get(i);
      for (int j = 0; j < columnes; j++) {
        String item = (String) fila.get(j);
        item.toLowerCase();

        // System.out.print(item);
        taulell[i][j] = item.charAt(0);
      }
      // System.out.println();
    }

    return taulell;
  }







  public void guardar(char[][] taulell) {

    int alcada = taulell.length;
    int amplada = taulell[0].length;

    String nomNivell = "facil";

    JSONArray nivell;

    if (alcada>MAX_MEDIA || amplada>MAX_MEDIA){ // ES DIFICIL
      nomNivell = "dificil";
      nivell = (JSONArray) nivells.get(2);
    } else if (alcada>MAX_FACIL || amplada>MAX_FACIL){ //ES MITJA
      nomNivell = "mitjà";
       nivell = (JSONArray) nivells.get(1);
    } else {  //ES FACIL
      nomNivell = "fàcil";
      nivell = (JSONArray) nivells.get(0);
    }

    JSONArray objTaulell = new JSONArray();
    for (int i=0; i<taulell.length; i++){
      JSONArray fila = new JSONArray();
      for (int j=0; j<taulell[0].length; j++){
        String entrada = String.valueOf(taulell[i][j]);
        fila.add(entrada);
      }
      objTaulell.add(fila);
    }
    nivell.add(objTaulell);


    /*
    JSONObject obj = new JSONObject();
    obj.put("Blog", "http://javainutil.blogspot.com");
    obj.put("Temas", "Informatica");
    obj.put("Inicio", new Integer(2012));

    JSONArray list = new JSONArray();
    list.add("tag 1");
    list.add("tag 2");
    list.add("tag 3");

    obj.put("Tags", list);

    JSONObject innerObj = new JSONObject();
    innerObj.put("PostX", "Escribir un JSON");
    innerObj.put("PostY", "Leer un JSON");
    innerObj.put("PostZ", "lalala");

    obj.put("Posts", innerObj);
*/

    try {
      FileWriter file = new FileWriter("res/taulells.json");
      file.write(nivells.toJSONString());
      file.flush();
      file.close();
    } catch (Exception e) {
      System.out.println("Error enmagatzemant taulell");
    }

    System.out.println("Taulell guardat com a nivell "+nomNivell+".");
    System.out.println();
    System.out.println();

  }


}