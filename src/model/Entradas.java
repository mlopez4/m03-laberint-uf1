package model;

import java.util.Scanner;

public class Entradas {


  /**
   * demana un enter i comprova que estigui entre els paramentres
   * @param min
   * @param max
   * @return
   */
  public static int demanarEnter(int min, int max){

    Scanner in = new Scanner(System.in);
    boolean correncte = false;
    int entrada = 0;

    while (correncte  == false) {

      if (in.hasNextInt()){
        entrada = in.nextInt();
        if (entrada>=min && entrada<=max){
          correncte = true;
        } else {
          System.out.println("Entrada incorrecta.");
          correncte = false;
        }
      } else {
        System.out.println("Introdueix un enter.");
        correncte = false;
        in.nextLine();
      }
    }

    return entrada;

  }


  /**
   * Demana un caracter i comproba que correspongui amb l'array entrat per parametre
   * @param opcions
   * @return
   */
  public static char demanarCaracter(char[] opcions){

    Scanner in = new Scanner(System.in);
    boolean correcte = false;
    char entrada ='0';

    while (correcte  == false) {
        String lineaEntrada = in.nextLine();
        lineaEntrada.toLowerCase();
        if (lineaEntrada.equalsIgnoreCase("")){
          correcte = false;
        } else {
          char dipositEntrada = lineaEntrada.charAt(0);
          for (int i=0; i< opcions.length; i++){
            if (dipositEntrada == opcions[i]){
              correcte = true;
              entrada = dipositEntrada;
            }
          }
        }

        if (correcte==false){
          System.out.println("Entrada incorrecta.");
        }
    }

    return entrada;

  }


  /**
   * demana un nom, ha de tenir menys de 10 caracters
   * @return
   */
  public static String demanarNom(){
    Scanner in = new Scanner(System.in);
    boolean correcte = false;
    String nom = null;

    while (correcte==false){
      String entrada =  in.nextLine();
      if (entrada.length()<10){
        nom = entrada;
        correcte = true;
      } else {
        System.out.println("El teu nom no pot tenir mes de 10 caracters.");
      }
    }


    return nom;
  }


  /**
   * demana un taulell i comproba que tingui mes de 4 files/columnes i que
   * no contingui cap caracter inadecuat.
   * @return
   */
  public static char[][] demanarTaulell(){
    System.out.println("Introdueix l'arçada: (De 4 a 20)");
    int alcada = demanarEnter(4,20);
    System.out.println("Introdueix l'amplada: (De 4 a 20)");
    int amplada = demanarEnter(4,20);
    char[][] taulell = new char[alcada][amplada];
    Scanner in = new Scanner(System.in);

    boolean correcte = false;
    while (correcte==false){
      System.out.println("Dibuixa el teu taulell:");
      boolean llargada = false;
      for (int i=0; i<alcada && llargada==false; i++){

        String s = "Linia "+(i+1)+"-";
        System.out.print(String.format("|%-15s|",s)); //INDICADOR DE LINIA ACTUAL

        String linia = in.nextLine().toLowerCase();
        if (linia.length()!= amplada){
          System.out.println("L'mplada de la linea "+(i+1)+" és incorrecta.");
          System.out.println("Si us plau, torna a començar");
          llargada = true;
        } else {
          for (int j=0; j<amplada; j++){
            char caracter = linia.charAt(j);
            taulell[i][j] = caracter;
          }
        }
      }
      correcte = comprobarTaulell(taulell);
    }

    return taulell;
  }


  /**
   * comproba els caracters d'un taulell
   * @param taulell
   * @return
   */
  private static boolean comprobarTaulell(char[][] taulell){

    boolean correcte = true;

    for (int i=0; i<taulell.length && correcte==true; i++){
      for (int j=0; j<taulell[0].length && correcte==true; j++){
        char item = taulell[i][j];
        if (item=='.' || item=='|' || item=='p' || item=='f'){
          correcte = true;
        } else {
          System.out.println("Caracter incorrecte: Linea "+(i+1)+" posició "+(j+1));
          correcte = false;
        }
      }
    }
    return correcte;
  }



}
