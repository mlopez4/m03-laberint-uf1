package model;

import org.ietf.jgss.GSSContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import view.General;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class GestionarPuntuacio {

  private JSONArray score;


  public GestionarPuntuacio (){
    JSONParser parser = new JSONParser();
    try {
      File fitxer = new File("res/score.json");
      FileReader f = new FileReader(fitxer);
      score = (JSONArray) parser.parse(f);
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Error de lectura");
    }
  }



  public void guardarPuntuacio(Partida p){
   int moviments = p.getMoviments();
   String nom = p.getNomJugador();
   String nivell = p.getNivell();

    JSONObject obj = new JSONObject();
    obj.put("nom", nom);
    obj.put("nivell", nivell);
    obj.put("moviments", moviments);

    score.add(obj);

    try {
      FileWriter file = new FileWriter("res/score.json");
      file.write(score.toJSONString());
      file.flush();
      file.close();
    } catch (Exception e) {
      System.out.println("Error enmagatzemant taulell");
    }
  }

  public void mostrarPuntuacions(){
    General.subtitol("Les millors puntuacions:");

    int cFacil=0, cMitja=0, cDificil=0;
    for (int i=0; i<score.size(); i++){
      JSONObject partida = (JSONObject)score.get(i);
      String nivell = (String)partida.get("nivell");
      switch (nivell){
        case "facil":
          cFacil++;
          break;
        case "mitja":
          cMitja++;
          break;
        case "dificil":
          cDificil++;
          break;
      }
    }

    JSONObject[] facil = new JSONObject[cFacil];
    int posF = 0;
    JSONObject[] mitja = new JSONObject[cMitja];
    int posM = 0;
    JSONObject[] dificil = new JSONObject[cDificil];
    int posD = 0;

    for (int i=0; i<score.size();i++){
      JSONObject partida = (JSONObject)score.get(i);

      switch ((String)partida.get("nivell")){
        case "facil":
          facil[posF] = partida;
          posF++;
          break;
        case "mitja":
          mitja[posM] = partida;
          posM++;
          break;
        case "dificil":
          dificil[posD] = partida;
          posD++;
          break;
      }
    }


    for (int i=0; i<facil.length; i++){
      JSONObject partidaI = facil[i];
      for (int j=0; j<facil.length; j++){
        JSONObject partidaJ = facil[j];
        if ((long)partidaI.get("moviments") < (long)partidaJ.get("moviments")){
          JSONObject diposit = facil[i];
          facil[i]=facil[j];
          facil[j]=diposit;
        }
      }
    }

    for (int i=0; i<mitja.length; i++){
      JSONObject partidaI = mitja[i];
      for (int j=0; j<mitja.length; j++){
        JSONObject partidaJ = mitja[j];
        if ((long)partidaI.get("moviments") < (long)partidaJ.get("moviments")){
          JSONObject diposit = mitja[i];
          mitja[i]=mitja[j];
          mitja[j]=diposit;
        }
      }
    }

    for (int i=0; i<dificil.length; i++){
      JSONObject partidaI = dificil[i];
      for (int j=0; j<dificil.length; j++){
        JSONObject partidaJ = dificil[j];
        if ((long)partidaI.get("moviments") < (long)partidaJ.get("moviments")){
          JSONObject diposit = dificil[i];
          dificil[i]=dificil[j];
          dificil[j]=diposit;
        }
      }
    }

    General.subtitol("PUNTUACIONS PER NIVELLS");

    for (int i=0; i<facil.length; i++){
      System.out.println("FACIL | Posicio "+(i+1)+": "+facil[i].get("nom")+" amb "+(long)facil[i].get("moviments")+" moviments");
    }
    System.out.println();
    for (int i=0; i<mitja.length; i++){
      System.out.println("MITJÀ | Posicio "+(i+1)+": "+mitja[i].get("nom")+" amb "+(long)mitja[i].get("moviments")+" moviments");
    }
    System.out.println();
    for (int i=0; i<dificil.length; i++){
      System.out.println("DIFICIL | Posicio "+(i+1)+": "+dificil[i].get("nom")+" amb "+(long)dificil[i].get("moviments")+" moviments");
    }

    System.out.println();
    System.out.println();


  }


}
