package model;

import view.General;

public class Partida {

  private char[][] taulell;
  private char[][] taulellJugador;

  private int posFila;
  private int posCol;

  private int moviments;

  private String nomJugador;

  private String nivell;


  /**
   * construcutor
   *
   * @param taulell
   * @param nom
   */
  public Partida(char[][] taulell, String nom){
    this.taulell = taulell;
    this.nomJugador = nom;
    moviments = 0 ;
    taulellJugador = new char[taulell.length][taulell[0].length];
    for (int i=0; i<taulellJugador.length; i++){
      for (int j=0; j<taulellJugador[0].length; j++){
        taulellJugador[i][j] = '*';
        if (taulell[i][j]=='p'){
          posFila = i;
          posCol = j;
          taulellJugador[i][j] = 'p';
        }
      }
    }

    if (taulell.length>GestionarTaulells.MAX_MEDIA || taulell[0].length>GestionarTaulells.MAX_MEDIA){ // ES DIFICIL
      nivell = "dificil";
    } else if (taulell.length>GestionarTaulells.MAX_FACIL || taulell[0].length>GestionarTaulells.MAX_FACIL){ //ES MITJA
      nivell = "mitja";
    } else {  //ES FACIL
      nivell = "facil";
    }
  }


  // GETTERS

  public char[][] getTaulell() {
    return taulell;
  }

  public char[][] getTaulellJugador() {
    return taulellJugador;
  }

  public int getPosFila() {
    return posFila;
  }

  public int getPosCol() {
    return posCol;
  }


  public int getMoviments() {
    return moviments;
  }

  public String getNomJugador() {
    return nomJugador;
  }

  public String getNivell() {
    return nivell;
  }

  /**
   * incrementa moviment
   */
  public void upMoviments(){
    moviments++;
  }

  /**
   * Fa visible la casella on es troba el jugador
   */
  public void fixarPosicio(){
    taulellJugador[posFila][posCol] = taulell[posFila][posCol];
  }



  //   MOVIMENTS

  public boolean dreta() {
    boolean fi = false;
    int pos = posCol+1;
    if (pos>taulell[0].length-1 || pos<0){
      General.subtitol("CAURÀS DEL TAULELL!!");
    } else {
      taulellJugador[posFila][pos] = taulell[posFila][pos];
      if (taulell[posFila][pos]=='|'){
        General.subtitol("OUTCH!!! UNA PARET, QUIN MAL!!");
      } else {
        if (taulell[posFila][pos]=='f'){
          fi = true;
        } else {
          posCol++;
          General.subtitol("MOLT BE! - Posicio: "+(posFila+1)+" - "+(posCol+1));
        }
      }
    }
    upMoviments();

    return fi;
  }

  public boolean esquerra(){
    boolean fi = false;
    int pos = posCol-1;
    if (pos>taulell[0].length-1 || pos<0){
      General.subtitol("CAURÀS DEL TAULELL!!");
    } else {
      taulellJugador[posFila][pos] = taulell[posFila][pos];
      if (taulell[posFila][pos]=='|'){
        General.subtitol("OUTCH!!! UNA PARET, QUIN MAL!!");
      } else {
        if (taulell[posFila][pos]=='f'){
          fi = true;
        } else {
          posCol--;
          General.subtitol("MOLT BE! - Posicio: "+(posFila+1)+" - "+(posCol+1));
        }
      }
    }
    upMoviments();
    return fi;
  }

  public boolean amunt(){
    boolean fi = false;
    int pos = posFila-1;
    if (pos>taulell.length-1 || pos<0){
      General.subtitol("CAURÀS DEL TAULELL!!");
    } else {
      taulellJugador[pos][posCol] = taulell[pos][posCol];
      if (taulell[pos][posCol]=='|'){
        General.subtitol("OUTCH!!! UNA PARET, QUIN MAL!!");
      } else {
        if (taulell[pos][posCol] == 'f'){
          fi = true;
        } else {
          posFila--;
          General.subtitol("MOLT BE! - Posicio: "+(posFila+1)+" - "+(posCol+1));
        }
      }
    }
    upMoviments();
    return  fi;
  }

  public boolean abaix(){
    boolean fi = false;
    int pos = posFila+1;
    if (pos>taulell.length-1 || pos<0){
      General.subtitol("CAURÀS DEL TAULELL!!");
    } else {
      taulellJugador[pos][posCol] = taulell[pos][posCol];
      if (taulell[pos][posCol]=='|'){
        General.subtitol("OUTCH!!! UNA PARET, QUIN MAL!!");
      } else {
        if (taulell[pos][posCol] == 'f'){
          fi = true;
        } else {
          posFila++;
          General.subtitol("MOLT BE! - Posicio: "+(posFila+1)+" - "+(posCol+1));
        }
      }
    }
    upMoviments();
    return fi;
  }


}
